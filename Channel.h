/*
**  Channel.h
**
**  Copyright (c) 2002
**
**  Author: Ludovic Marcotte <ludovic@Sophos.ca>
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#import <AppKit/AppKit.h>

#import "Element.h"

@class GSXMLNode;
@class Plop;
@class PlopWindowController;

@interface Channel : Element
{
  NSMutableArray *allElements;
  NSImage *icon;

  Plop *plop;

  PlopWindowController *plopWindowController;
  NSTimer *animation_timer, *timer;

  int animation_index;

  NSURLHandle	*inProgress;
}


//
// access / mutation methods
//
- (void) addElement: (Element *) theElement;
- (void) removeElement: (Element *) theElement;

- (void) removeAllElements;

- (int) count;
- (int) itemCount;

- (Element *) elementAtIndex: (int) theIndex;

- (NSImage *) icon;
- (void) setIcon: (NSImage *) theIcon;

- (Plop *) plop;
- (void) setPlop: (Plop *) thePlop;

- (PlopWindowController *) plopWindowController;
- (void) setPlopWindowController: (PlopWindowController *) theController;


//
// Other methods
//
- (void) update: (id) sender;
- (void) updateAnimation;

//
// Static method
//

@end



//
// Private methods
//
@interface Channel (Private)

- (void) _parseNodeInformationUsingElement: (Element *) theElement
                                      node: (id) theNode;

@end
