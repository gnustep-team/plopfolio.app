/*
**  PlopWindow.m
**
**  Copyright (c) 2002
**
**  Author: Ludovic Marcotte <ludovic@Sophos.ca>
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#import "PlopWindow.h"

#import "Constants.h"
#import "PlopView.h"

@implementation PlopWindow


//
//
//
- (id) initWithContentRect: (NSRect) contentRect 
		 styleMask: (unsigned int) styleMask
		   backing: (NSBackingStoreType) backingType
		     defer: (BOOL) flag
{
  self = [super initWithContentRect: contentRect
		styleMask: styleMask
		backing: backingType
		defer: flag];

  [self setPreviousSize: NSMakeSize(DEFAULT_PLOP_WIDTH, DEFAULT_PLOP_HEIGHT)];

  return self;
}

//
//
//
- (void) dealloc
{
  //NSLog(@"PlopWindow: -dealloc");
  RELEASE(plopView);

  RELEASE(textView);
  RELEASE(scrollView);
  
  [super dealloc];
}


//
//
//
- (void) layoutWindow
{
  plopView = [[PlopView alloc] init];
  [plopView setFrame: NSMakeRect(0,0,DEFAULT_PLOP_WIDTH,DEFAULT_PLOP_HEIGHT)];
  [plopView setAutoresizingMask: (NSViewWidthSizable|NSViewHeightSizable)];
  [plopView setWindow: self];
  
  scrollView = [[NSScrollView alloc] initWithFrame: NSMakeRect(30,14,DEFAULT_PLOP_WIDTH-6,DEFAULT_PLOP_HEIGHT - 25)];
  
  textView = [[NSTextView alloc] initWithFrame: [[scrollView contentView] frame]];
  //[textView setTextContainerInset: NSMakeSize(5,5)];
  [textView setBackgroundColor: [NSColor whiteColor]];
  [textView setDrawsBackground: YES];
  [textView setRichText: YES];
  [textView setUsesFontPanel: YES];
  [textView setHorizontallyResizable: NO];
  [textView setVerticallyResizable: YES];
  [textView setMinSize: NSMakeSize (0, 0)];
  [textView setMaxSize: NSMakeSize (1E7, 1E7)];
  [textView setAutoresizingMask: NSViewHeightSizable|NSViewWidthSizable];
  [[textView textContainer] setContainerSize: NSMakeSize([[scrollView contentView] frame].size.width, 
  							 1E7)];
  [[textView textContainer] setWidthTracksTextView: YES];
  [textView setEditable: NO];
  [textView setDelegate: [self windowController]];

  [scrollView setDocumentView: textView];
  [scrollView setHasHorizontalScroller: NO];
  [scrollView setHasVerticalScroller: NO];
  [scrollView setBorderType: NSNoBorder];
  [scrollView setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
  
  [[self contentView] addSubview: plopView];
  [[self contentView] addSubview: scrollView];
}


//
//
//
- (BOOL) canBecomeKeyWindow
{
  return YES;
}


//
// access/mutation methods
//
- (NSSize) maxSize
{
  NSRect aRect;
  
  aRect = [[NSScreen mainScreen] frame];
  
  return NSMakeSize(aRect.size.width - 140, aRect.size.height - 140);
}

- (NSSize) minSize
{
  return NSMakeSize(36,40);
}


- (NSSize) previousSize
{
  return previousSize;
}

- (void) setPreviousSize: (NSSize) theSize
{
  previousSize = theSize;
}


- (PlopView *) plopView
{
  return plopView;
}


- (NSTextView *) textView
{
  return textView;
}

@end
