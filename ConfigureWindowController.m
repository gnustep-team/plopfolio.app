/*
**  ConfigureWindowController.m
**
**  Copyright (c) 2002
**
**  Author: Ludovic Marcotte <ludovic@Sophos.ca>
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#import "ConfigureWindowController.h"

#import "Constants.h"

#ifndef MACOSX
#import "ConfigureWindow.h"
#endif

#import "Plop.h"

static id lastConfigureWindowOnTop = nil;


@implementation ConfigureWindowController

//
//
//
- (id) initWithWindowNibName: (NSString *) theNibName
{
#ifdef MACOSX
  self = [super initWithWindowNibName: theNibName];
#else
  ConfigureWindow *configureWindow;
  
  configureWindow = [[ConfigureWindow alloc] initWithContentRect:NSMakeRect(10,10,236,310)
					styleMask: NSClosableWindowMask
					backing: NSBackingStoreBuffered
					defer: NO];

  self = [super initWithWindow: configureWindow];
  RELEASE(configureWindow);
 
  [configureWindow layoutWindow];
  [configureWindow setDelegate: self];

  // We link our outlets
  imageView = [configureWindow imageView];
  textView = [configureWindow textView];
#endif

  // We set the title to an empty string
  [[self window] setTitle: _(@"Configure a Plop...")];
 
  return self;
}


//
//
//
- (void) dealloc
{
  NSLog(@"ConfigureWindowController: -dealloc");

  RELEASE(plop);

  [super dealloc];
}


//
//
//
- (void) windowDidLoad
{
  lastConfigureWindowOnTop = [self window];
  
  [super windowDidLoad];
}

//
//
//
- (BOOL) windowShouldClose: (id) sender
{
  return YES;
}


//
//
//
- (void) windowWillClose: (NSNotification *) theNotification
{
  AUTORELEASE(self);
}


//
// access / mutation
//
- (Plop *) plop
{
  return plop;
}


//
//
//
- (void) setPlop: (Plop *) thePlop
{
  if ( thePlop )
    {
      NSMutableString *aMutableString;

      RETAIN(thePlop);
      RELEASE(plop);
      plop = thePlop;

      // We load our banner image
      if ( [plop bannerSource] )
	{
	  NSData *aData;
	  
	  aData = [[plop bannerSource] resourceDataUsingCache: NO];

	  if ( aData )
	    {
	      NSString *aString;

	      aString = [NSString stringWithFormat: @"%@/bannerSource_%d.gif",
				  NSTemporaryDirectory(), 
				  [[NSProcessInfo processInfo] processIdentifier]];
	      
	      if ( [aData writeToFile: aString
			  atomically: YES] )
		{
		  NSImage *aImage;

		  aImage = [[NSImage alloc] initWithContentsOfFile: aString];

		  if ( aImage )
		    {
		      [imageView setImage: aImage];
		      [imageView setNeedsDisplay: YES];
		      
		      RELEASE(aImage);
		    }
		  else
		    {
		      NSLog(@"Unable to create the image from the content of the file.");
		    }
		  
		  [[NSFileManager defaultManager] removeFileAtPath: aString
		  				  handler: nil];
		}
	    }
	  else
	    {
	      NSLog(@"Unable to obtain the banner for the URL.");
	    }
	} // if ( [plop bannerSource] )
      else
	{
	  NSLog(@"No banner was specified in this Plop.");
	}

      // We set the content of our text view
      aMutableString = [[NSMutableString alloc] init];
      [aMutableString appendFormat: @"%@\n", [plop title]];
      [aMutableString appendFormat: @"Version %@\n\n", [plop plopVersion]];
      [aMutableString appendFormat: @"%@\n\n", [plop description]];
      [aMutableString appendFormat: @"Refresh Rate:\n%d minutes.", [plop refresh]];

      [textView setString: aMutableString];
      RELEASE(aMutableString);
    }
  else
    {
      DESTROY(plop);
    }
}


@end


//
// private methods
//
@implementation ConfigureWindowController (Private)



@end
