/*
**  PreferencesPanel.m
**
**  Copyright (c) 2002
**
**  Author: Ludovic Marcotte <ludovic@Sophos.ca>
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#import "PreferencesPanel.h"

#import "Constants.h"
#import "ExtendedTableColumn.h"
#import "ExtendedTableView.h"
#import "LabelWidget.h"
#import "PreferencesPanelController.h"

@implementation PreferencesPanel

//
//
//
- (void) dealloc
{
  NSLog(@"PreferencesPanel: -dealloc");

  RELEASE(tableView);
  RELEASE(browserPopUpButton);
  RELEASE(browserField);

  [super dealloc];
}


//
//
//
- (void) layoutPanel
{
  ExtendedTableColumn *activeTableColumn, *nameTableColumn;
  ExtendedButtonCell *cell;

  NSScrollView *scrollView;
  NSTabViewItem *item;

  NSButton *configure, *delete, *import;
  LabelWidget *label, *browserLabel, *browserExampleLabel;
  NSBox *box;

  tabView = [[NSTabView alloc] initWithFrame: NSMakeRect(0,0,355,270)];
  [tabView setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];

  //
  // First tab view item
  //
  item = [[NSTabViewItem alloc] initWithIdentifier: @"Plops"];
  [item setLabel: _(@"Plops")];
  
  box = [[NSBox alloc] initWithFrame: NSMakeRect(0,0,355,270)];
  [box setBorderType: NSNoBorder];
  [box setTitlePosition: NSNoTitle];
  [box setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
  [item setView: box];
  RELEASE(box);

  // Label
  label = [LabelWidget labelWidgetWithFrame: NSMakeRect(5,220,345,TextFieldHeight)
		       label: _(@"Your Plops:")];
  [label setAutoresizingMask: NSViewMaxXMargin|NSViewMaxYMargin];
  [[item view] addSubview: label];
  

  // Tableview
  activeTableColumn = [[ExtendedTableColumn alloc] initWithIdentifier: @"Active"];
  [activeTableColumn setEditable: YES];
  [[activeTableColumn headerCell] setStringValue: _(@"Active")];
  [activeTableColumn setWidth: 60];

  cell = [[ExtendedButtonCell alloc] init];
  [cell setButtonType: NSSwitchButton];
  [cell setImagePosition: NSImageOnly];
  [activeTableColumn setDataCell: cell];
  RELEASE(cell);
  
  [activeTableColumn setShouldUseAndSetState: YES];
  [activeTableColumn setShouldUseMouse: YES];

  nameTableColumn = [[ExtendedTableColumn alloc] initWithIdentifier: @"Name"];
  [nameTableColumn setEditable: YES];
  [[nameTableColumn headerCell] setStringValue: _(@"Name")];

  tableView = [[ExtendedTableView alloc] initWithFrame: NSMakeRect(5,40,335,185)];
  [tableView setDrawsGrid: NO];
  [tableView setAllowsColumnSelection: NO];
  [tableView setAllowsColumnReordering: NO];
  [tableView setAllowsEmptySelection: NO];
  [tableView setAllowsMultipleSelection: NO];
  [tableView addTableColumn: activeTableColumn];
  [tableView addTableColumn: nameTableColumn];
  [tableView setDataSource: [self windowController]]; 
  [tableView setDelegate: [self windowController]];

  RELEASE(activeTableColumn);
  RELEASE(nameTableColumn);

  scrollView = [[NSScrollView alloc] initWithFrame: NSMakeRect(5,40,335,185)];
  [scrollView setHasHorizontalScroller: NO];
  [scrollView setHasVerticalScroller: YES];
  [scrollView setDocumentView:tableView];
  [scrollView setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
  [[item view] addSubview: scrollView];
  RELEASE(scrollView);

  // Delete button
  delete = [[NSButton alloc] initWithFrame: NSMakeRect(5,5,80,ButtonHeight)];
  [delete setTitle: _(@"Delete")];
  [delete setTarget: [self windowController]];
  [delete setAction: @selector(deleteClicked:)];
  [delete setAutoresizingMask: NSViewMaxXMargin|NSViewMaxYMargin];
  [[item view] addSubview: delete];
  RELEASE(delete);

  // Configure button
  configure = [[NSButton alloc] initWithFrame: NSMakeRect(90,5,80,ButtonHeight)];
  [configure setTitle: _(@"Configure")];
  [configure setTarget: [self windowController]];
  [configure setAction: @selector(configureClicked:)];
  [configure setAutoresizingMask: NSViewMaxXMargin|NSViewMaxYMargin];
  [[item view] addSubview: configure];
  RELEASE(configure);
  
  // Import button
  import = [[NSButton alloc] initWithFrame: NSMakeRect(220,5,120,ButtonHeight)];
  [import setTitle: _(@"Import a Klip")];
  [import setTarget: [self windowController]];
  [import setAction: @selector(importClicked:)];
  [import setAutoresizingMask: NSViewMinXMargin|NSViewMaxYMargin];
  [[item view] addSubview: import];
  RELEASE(import);

  [tabView addTabViewItem: item];
  RELEASE(item);


  //
  // Display
  //
  item = [[NSTabViewItem alloc] initWithIdentifier: @"Display"];
  [item setLabel: _(@"Display")];
  
  box = [[NSBox alloc] initWithFrame: NSMakeRect(0,0,355,270)];
  [box setBorderType: NSNoBorder];
  [box setTitlePosition: NSNoTitle];
  [box setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
  [item setView: box];
  RELEASE(box);

  [tabView addTabViewItem: item];
  RELEASE(item);


  //
  // General
  //
  item = [[NSTabViewItem alloc] initWithIdentifier: @"General"];
  [item setLabel: _(@"General")];
  
  box = [[NSBox alloc] initWithFrame: NSMakeRect(0,0,355,270)];
  [box setBorderType: NSNoBorder];
  [box setTitlePosition: NSNoTitle];
  [box setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
  [item setView: box];
  RELEASE(box);

  browserLabel = [LabelWidget labelWidgetWithFrame: NSMakeRect(5,220,335,TextFieldHeight)
			      label: _(@"Open links using the following web browser:")];
  [browserLabel setAutoresizingMask: NSViewMaxXMargin|NSViewMaxYMargin];
  [[item view] addSubview: browserLabel];

  browserPopUpButton = [[NSPopUpButton alloc] initWithFrame: NSMakeRect(5,190,335,ButtonHeight)];
  [browserPopUpButton addItemWithTitle: _(@"Operating System Default")];
  [browserPopUpButton addItemWithTitle: _(@"Custom")];
  [browserPopUpButton setTarget: [self windowController]];
  [browserPopUpButton setAction: @selector(selectionInBrowserPopUpButtonHasChanged:)];
  [[item view] addSubview: browserPopUpButton];

  browserField = [[NSTextField alloc] initWithFrame: NSMakeRect(5,160,335,TextFieldHeight)];
  [[item view] addSubview: browserField];

  browserExampleLabel = [LabelWidget labelWidgetWithFrame: NSMakeRect(5,130,335,TextFieldHeight)
			      label: _(@"Example: mozilla -noraise -remote \"openURL(%@,new-window)\"")];
  [browserExampleLabel setAutoresizingMask: NSViewMaxXMargin|NSViewMaxYMargin];
  [browserExampleLabel setFont: [NSFont systemFontOfSize: 10]];
  [[item view] addSubview: browserExampleLabel];

  [tabView addTabViewItem: item];
  RELEASE(item);



  [[self contentView] addSubview: tabView];
}
 

//
// access/mutation methods
//
- (NSTableView *) tableView
{
  return tableView;
}

- (NSPopUpButton *) browserPopUpButton
{
  return browserPopUpButton;
}

- (NSTextField *) browserField
{
  return browserField;
}

@end
