/*
**  Plop.m
**
**  Copyright (c) 2002
**
**  Author: Ludovic Marcotte <ludovic@Sophos.ca>
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#import "Plop.h"

#import "Channel.h"
#import "Constants.h"

static int currentVersion = 1;

@implementation Plop 

//
//
//
- (id) init
{
  self = [super init];

  // We give a defaut size to our frame
  [self setActive: NO];
  [self setFrame: NSMakeRect(10,10,DEFAULT_PLOP_WIDTH,DEFAULT_PLOP_HEIGHT)];
  [self setRefresh: 600];

  [self setBannerSource: nil];
  [self setContentSource: nil];
  [self setChannel: nil];

  return self;
}


//
//
//
- (void) dealloc
{
  RELEASE(bannerSource);
  RELEASE(contentSource);
  RELEASE(defaultLink);
  RELEASE(description);
  RELEASE(iconSource);
  RELEASE(iconName);
  RELEASE(plopVersion);

  [super dealloc];
}


//
// coding protocol
//
- (void) encodeWithCoder: (NSCoder *) theCoder
{
  [Plop setVersion: currentVersion];

  [theCoder encodeObject: [NSNumber numberWithBool: [self isActive]] ];
  [theCoder encodeObject: [self bannerSource]];
  [theCoder encodeObject: [self contentSource]];
  [theCoder encodeObject: [self defaultLink]];
  [theCoder encodeObject: [self description]];
  [theCoder encodeObject: [self iconSource]];
  [theCoder encodeObject: [self iconName]];
  [theCoder encodeObject: [self title]];
  [theCoder encodeObject: [NSNumber numberWithInt: [self refresh]] ];
  [theCoder encodeObject: [self plopVersion]];

  // We finally encode our frame
  [theCoder encodeObject: [NSNumber numberWithFloat: frame.origin.x] ];
  [theCoder encodeObject: [NSNumber numberWithFloat: frame.origin.y] ];
  [theCoder encodeObject: [NSNumber numberWithFloat: frame.size.width] ];
  [theCoder encodeObject: [NSNumber numberWithFloat: frame.size.height] ];
}


//
//
//
- (id) initWithCoder: (NSCoder *) theCoder
{
  int aVersion;
  
  aVersion = [theCoder versionForClassName: NSStringFromClass([self class])];;

  self = [self init];

  // Version 1 - Corresponds to PlopFolio v0.1
  if ( aVersion == 1 )
    {
      [self setActive: [[theCoder decodeObject] boolValue]];
      [self setBannerSource: [theCoder decodeObject]];
      [self setContentSource: [theCoder decodeObject]];
      [self setDefaultLink: [theCoder decodeObject]];
      [self setDescription: [theCoder decodeObject]];
      [self setIconSource: [theCoder decodeObject]];
      [self setIconName: [theCoder decodeObject]];
      [self setTitle: [theCoder decodeObject]];
      [self setRefresh: [[theCoder decodeObject] intValue]];
      [self setPlopVersion: [theCoder decodeObject]];

      // We finally decode our frame
      frame.origin.x = [[theCoder decodeObject] floatValue];
      frame.origin.y = [[theCoder decodeObject] floatValue];
      frame.size.width = [[theCoder decodeObject] floatValue];
      frame.size.height = [[theCoder decodeObject] floatValue];
    }
  
  return self;
}


//
// access / mutation methods
//
- (BOOL) isActive
{
  return active;
}

- (void) setActive: (BOOL) theBOOL
{
  active = theBOOL;
}


//
//
//
- (NSURL *) bannerSource
{
  return bannerSource;
}

- (void) setBannerSource: (NSURL *) theBannerSource
{
  RETAIN(theBannerSource);
  RELEASE(bannerSource);
  bannerSource = theBannerSource;
}

//
//
//
- (NSURL *) iconSource
{
  return iconSource;
}

- (void) setIconSource: (NSURL *) theIconSource
{
  RETAIN(theIconSource);
  RELEASE(iconSource);
  iconSource = theIconSource;
}


//
//
//
- (NSString *) iconName
{
  return iconName;
}

- (void) setIconName: (NSString *) theName
{
  RETAIN(theName);
  RELEASE(iconName);
  iconName = theName;
}


//
//
//
- (NSURL *) contentSource
{
  return contentSource;
}

- (void) setContentSource: (NSURL *) theContentSource
{
  RETAIN(theContentSource);
  RELEASE(contentSource);
  contentSource = theContentSource;
}


//
//
//
- (NSURL *) defaultLink
{
  return defaultLink;
}

- (void) setDefaultLink: (NSURL *) theDefaultLink
{
  RETAIN(theDefaultLink);
  RELEASE(defaultLink);
  defaultLink = theDefaultLink;
}


//
//
//
- (NSString *) description
{
  return description;
}

- (void) setDescription: (NSString *) theDescription
{
  RETAIN(theDescription);
  RELEASE(description);
  description = theDescription;
}

//
//
//
- (NSRect) frame
{
  return frame;
}

- (void) setFrame: (NSRect) theFrame
{
  frame = theFrame;
}

//
//
//
- (int) refresh
{
  return refresh;
}

- (void) setRefresh: (int) theRefresh
{
  if ( theRefresh <= 0 )
    {
      theRefresh = 600;
    }

  refresh = theRefresh;
}


//
//
//
- (NSString *) title
{
  return title;
}

- (void) setTitle: (NSString *) theTitle
{
  RETAIN(theTitle);
  RELEASE(title);
  title = theTitle;
}


//
//
//
- (Channel *) channel
{
  return channel;
}


// No need to retain the channel here since the channel
// already retains the Plop.
- (void) setChannel: (Channel *) theChannel
{
  channel = theChannel;
}


//
//
//
- (NSString *) plopVersion
{
  return plopVersion;
}

- (void) setPlopVersion: (NSString *) thePlopVersion
{
  RETAIN(thePlopVersion);
  RELEASE(plopVersion);
  plopVersion = thePlopVersion;
}


//
// Other methods
//
- (void) cache
{
  if ( [self iconSource] )
    {
      NSData *aData;
      
      aData = [iconSource resourceDataUsingCache: NO];
      
      if ( aData )
	{
	  [self setIconName: [NSString stringWithFormat: @"%@_%@", [self title],
				       [[iconSource path] lastPathComponent]] ];
	  
	  [aData writeToFile: [NSString stringWithFormat: @"%@/%@", PlopFolioUserLibraryPath(),
					[self iconName]]
		 atomically: YES];
	}
    }
}

@end
