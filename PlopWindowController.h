/*
**  PlopWindowController.m
**
**  Copyright (c) 2002
**
**  Author: Ludovic Marcotte <ludovic@Sophos.ca>
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#import <AppKit/AppKit.h>

// Because of bug #2751274, on Mac OS X 10.1.x, we set up a #define here
// for the AppKit version that is fixed so that the code can automatically switch
// over to doing things the right way when the time comes.
#define kFixedDockMenuAppKitVersion 632

@class Channel;
@class PlopView;

@interface PlopWindowController : NSWindowController
{
  // Outlets
  IBOutlet NSTextView *textView;
  
  // Other ivar
  Channel *channel;
  NSMenu *dockMenu;
  NSMenu *dockSubMenu;
}

- (id) initWithWindowNibName: (NSString *) theNibName;
- (void) dealloc;


//
// delegate methods
//
- (void) windowDidBecomeKey: (NSNotification *) aNotification;
- (void) windowDidLoad;
- (void) windowDidMove: (NSNotification *) aNotification;
- (void) windowDidResize: (NSNotification *) aNotification;
- (void) windowWillClose: (NSNotification *) theNotification;

//
// action methods
//
#ifdef MACOSX
- (IBAction) dockItemSelected: (id) sender;
- (IBAction) dockItemUpdate: (id) sender;
#endif

//
// access / mutation
//
+ (id) lastPlopWindowOnTop;
- (NSTextView *) textView;
- (PlopView *) plopView;

- (Channel *) channel;
- (void) setChannel: (Channel *) theChannel;


//
// Other methods
//
- (BOOL) openURL: (NSURL *) theURL;
- (void) update;

@end

//
// private methods
//
@interface PlopWindowController (Private)

- (void) _updateWindowFrame;
#ifdef MACOSX
- (NSMenuItem *) _constructMenuItem: (NSString *) theTitle action: (SEL) theSelector url: (NSURL *) theURL;
#endif

@end
