

#import <Foundation/Foundation.h>

#define UNKNOWN   1
#define CHANNEL   1
#define IMAGE     2
#define ITEM      3
#define TEXTINPUT 4


@interface Element : NSObject
{
  NSString *title, *description;
  NSURL *link, *url;
  
  int type;
}

- (id) init;
- (void) dealloc;


//
// access / mutation methods
//
- (NSString *) title;
- (void) setTitle: (NSString *) theTitle;


- (NSString *) description;
- (void) setDescription: (NSString *) theDescription;

- (NSURL *) link;
- (void) setLink: (NSURL *) theLink;

- (NSURL *) url;
- (void) setURL: (NSURL *) theURL;

- (int) type;
- (void) setType: (int) theType;


@end
