/*
**  PlopView.m
**
**  Copyright (c) 2002
**
**  Author: Ludovic Marcotte <ludovic@Sophos.ca>
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#import "PlopView.h"

#import "Channel.h"
#import "Constants.h"
#import "Plop.h"
#import "PlopWindow.h"
#import "PlopWindowController.h"

@implementation PlopView

//
//
//
- (id) init
{
  self = [super init]; 

  resize = [NSImage imageNamed: @"resize.tiff"];
  [self setReloadIcon: [NSImage imageNamed: @"reload.tiff"]];

  icon = nil;
  animationIcon = nil;
  count = 0;

  titleBarColor = [NSColor colorWithDeviceRed: 0.75
			   green: 0.75
			   blue: 0.75
			   alpha: 1.0];
  
  plopColor = [NSColor colorWithDeviceRed: 0.90
		       green: 0.90
		       blue: 0.90
		       alpha: 1.0];
  

  RETAIN(resize);
  RETAIN(titleBarColor);
  RETAIN(plopColor);
  
  return self;
}


//
//
//
- (void) dealloc
{
  RELEASE(reload);
  RELEASE(resize);
  RELEASE(icon);

  RELEASE(titleBarColor);
  RELEASE(plopColor);

  [super dealloc];
}


//
//
//
- (BOOL) acceptsFirstResponder
{
  return YES;
}


//
//
//
- (BOOL) acceptsFirstMouse: (NSEvent *) theEvent
{
  return YES;
}


//
//
//
- (void) drawRect: (NSRect) theRect
{
  NSMutableAttributedString *title;
  NSFont *aFont;

  // We fill our rect with the color of our title bar's image for the buttons
  [titleBarColor set];
  NSRectFill(theRect);

  // Our reload button
  [reload compositeToPoint: NSMakePoint(theRect.size.width - 23, theRect.size.height - 11)
	  operation: NSCompositeSourceAtop];

  // Our animation icon, if we need to draw it
  if ( animationIcon )
    {
      //NSLog(@"drawing animation icon");
      [animationIcon compositeToPoint: NSMakePoint(theRect.size.width - 63, theRect.size.height - 11)
		     operation: NSCompositeSourceAtop];
    }

  // We now fill it with the color of the content of the plop
  [plopColor set];
  NSRectFill( NSMakeRect(0,0,theRect.size.width, theRect.size.height - 11) );

  // Our 16x16 icon
  if ( icon )
    {
      [icon compositeToPoint: NSMakePoint(4, theRect.size.height - 32)
	    operation: NSCompositeSourceAtop];
    }

  // Our bottom (resize) image
  [resize compositeToPoint: NSMakePoint(theRect.size.width - 14, 0)
	  operation: NSCompositeSourceAtop];

  // We draw the count title
  aFont = [NSFont boldSystemFontOfSize: 16];
  title = [[NSMutableAttributedString alloc]
	    initWithString: [NSString stringWithFormat: @"%d", count]];
  
  [title addAttribute: NSFontAttributeName
	 value: aFont
	 range: NSMakeRange(0, [[title string] length])];

  [title addAttribute: NSForegroundColorAttributeName
	 value: [NSColor blackColor]
	 range: NSMakeRange(0, [[title string] length])];
  

  [title drawAtPoint: NSMakePoint( (16-[title size].width)/2 + 4, 
				   theRect.size.height - 60)];
}


//
//
//
- (void) mouseDown: (NSEvent *) theEvent
{
  NSRect windowRect, resizeRect;
  NSPoint bottomRightOrigin;
  
  NSEvent *nextEvent;
  BOOL resizing;

  lastLocationOfWindow = [theEvent locationInWindow];

  windowRect = [window frame];

  // We get the point corresponding to the origin of our bottom right corner
  bottomRightOrigin = [window convertBaseToScreen: lastLocationOfWindow];

  //NSLog(@"%@", NSStringFromRect(windowRect));

  resizeRect = NSMakeRect(windowRect.size.width - 8, 0, 8, 8);
  resizing = NSMouseInRect(lastLocationOfWindow, resizeRect, NO);

  //NSLog(@"Resizing = %d", resizing);

  while (YES) 
    {
      nextEvent = [window nextEventMatchingMask: NSLeftMouseUpMask | NSLeftMouseDraggedMask];
     
      if ( [nextEvent type] == NSLeftMouseUp )
	{
	  [self mouseUp: theEvent];
	  break;
	}
      else if ( [nextEvent type] == NSLeftMouseDragged )
	{
	  NSPoint aPoint;
	  
	  aPoint = [window mouseLocationOutsideOfEventStream];

	  if ( !resizing )
	    {
	      NSPoint origin;
	      
	      
	      origin = [window frame].origin;
	      origin.x += (aPoint.x - lastLocationOfWindow.x);
	      origin.y += (aPoint.y - lastLocationOfWindow.y);
	      
	      [window setFrameOrigin: origin];
	    }
	  else
	    {
	      NSRect aRect;
	      float dx, dy;

	      aPoint = [window convertBaseToScreen: aPoint];
	  
	      dx = (aPoint.x - bottomRightOrigin.x);
	      dy = (aPoint.y - bottomRightOrigin.y);
		
	      aRect = NSMakeRect(windowRect.origin.x,
				 windowRect.origin.y + dy,
				 windowRect.size.width + dx,
				 windowRect.size.height - dy);
	      
	      // We don't resize if we reached our minimum size...
	      if (aRect.size.width < [window minSize].width)
		{
		  aRect.size.width = [window minSize].width;
		}

	      if (aRect.size.height < [window minSize].height)
		{
		  aRect.origin.y -= [window minSize].height - aRect.size.height;
		  aRect.size.height = [window minSize].height;
		}

	      if ( aRect.size.width >= [window minSize].width &&
		   aRect.size.height >= [window minSize].height )
		{
		  [window setFrame: aRect
			  display: YES];
		}
	    }
	}
    }
}


//
// Method used to detect when the user release the left mouse button 
// in order to know if we must minimize, maximize or close the window.
//
- (void) mouseUp: (NSEvent *) theEvent
{
  NSRect iconRect, reloadRect, windowRect;
  NSPoint aPoint;

  aPoint = [theEvent locationInWindow];
  windowRect = [window frame];

  iconRect = NSMakeRect(4,windowRect.size.height - 32,16,16);
  reloadRect = NSMakeRect(windowRect.size.width - 23, windowRect.size.height - 10, 23, 7);
  
  //NSLog(@"Location = %f %f", aPoint.x, aPoint.y);

  if ( NSMouseInRect(aPoint, reloadRect, NO) )
    {
      [(Channel *)[[(PlopWindow *)window windowController] channel] update: self];
    }
  else if ( NSMouseInRect(aPoint, iconRect, NO) )
    {
      Plop *aPlop;
      
      aPlop = [[[(PlopWindow *)window windowController] channel] plop];

      if ( [aPlop defaultLink] )
	{
	  [[(PlopWindow *)window windowController] openURL: [aPlop defaultLink]];
	}
    }
}


//
// access/mutation methods
//
- (void) setWindow: (NSWindow *) theWindow
{
  window = theWindow;
}

- (void) setIcon: (NSImage *) theIcon
{
  RETAIN(theIcon);
  RELEASE(icon);
  icon = theIcon;
}

- (void) setCount: (int) theCount
{
  count = theCount;
}

- (void) setAnimationIcon: (NSImage *) theIcon
{
  RETAIN(theIcon);
  RELEASE(animationIcon);
  animationIcon = theIcon;
}

- (void) setReloadIcon: (NSImage *) theIcon
{
  RETAIN(theIcon);
  RELEASE(reload);
  reload = theIcon;
}

@end
