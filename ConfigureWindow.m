/*
**  ConfigureWindow.m
**
**  Copyright (c) 2002
**
**  Author: Ludovic Marcotte <ludovic@Sophos.ca>
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#import "ConfigureWindow.h"

#import "Constants.h"

@implementation ConfigureWindow

//
//
//
- (void) dealloc
{
  NSLog(@"ConfigureWindow: -dealloc");
  
  RELEASE(imageView);
  RELEASE(textView);
  
  [super dealloc];
}


//
//
//
- (void) layoutWindow
{
  NSScrollView *scrollView;

  imageView = [[NSImageView alloc] initWithFrame:NSMakeRect(0,250,236,60)];
  [imageView setImageScaling: NSScaleToFit];


  scrollView = [[NSScrollView alloc] initWithFrame: NSMakeRect(5,5,224,240)];
  
  textView = [[NSTextView alloc] initWithFrame: [[scrollView contentView] frame]];
  //[textView setTextContainerInset: NSMakeSize(5,5)];
  [textView setBackgroundColor: [NSColor whiteColor]];
  [textView setDrawsBackground: YES];
  [textView setRichText: NO];
  [textView setUsesFontPanel: NO];
  [textView setHorizontallyResizable: NO];
  [textView setVerticallyResizable: YES];
  [textView setMinSize: NSMakeSize (0, 0)];
  [textView setMaxSize: NSMakeSize (1E7, 1E7)];
  [textView setAutoresizingMask: NSViewHeightSizable|NSViewWidthSizable];
  [[textView textContainer] setContainerSize: NSMakeSize([[scrollView contentView] frame].size.width, 
  							 1E7)];
  [[textView textContainer] setWidthTracksTextView: YES];
  [textView setEditable: NO];
  [textView setDelegate: [self windowController]];

  [scrollView setDocumentView: textView];
  [scrollView setHasHorizontalScroller: NO];
  [scrollView setHasVerticalScroller: YES];
  [scrollView setBorderType: NSLineBorder];
  [scrollView setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
  
  [[self contentView] addSubview: imageView];
  [[self contentView] addSubview: scrollView];
  RELEASE(scrollView);
}


//
// access/mutation methods
//
- (NSImageView *) imageView
{
  return imageView;
}

- (NSTextView *) textView
{
  return textView;
}

@end
