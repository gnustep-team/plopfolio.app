
#import "Element.h"

#import "Constants.h"

@implementation Element

//
//
//
- (id) init
{
  self = [super init];

  
  return self;
}


//
//
//
- (void) dealloc
{
  RELEASE(title);
  RELEASE(description);
  RELEASE(link);
  RELEASE(url);

  [super dealloc];
}


//
// access / mutation methods
//
- (NSString *) title
{
  return title;
}


- (void) setTitle: (NSString *) theTitle
{
  RETAIN(theTitle);
  RELEASE(title);
  title = theTitle;
}


- (NSString *) description
{
  return description;
}

- (void) setDescription: (NSString *) theDescription
{
  RETAIN(theDescription);
  RELEASE(description);
  description = theDescription;
}

- (NSURL *) link
{
  return link;
}

- (void) setLink: (NSURL *) theLink
{
  RETAIN(theLink);
  RELEASE(link);
  link = theLink;
}

- (NSURL *) url
{
  return url;
}

- (void) setURL: (NSURL *) theURL
{
  RETAIN(theURL);
  RELEASE(url);
  url = theURL;
}

- (int) type
{
  return type;
}

- (void) setType: (int) theType
{
  type = theType;
}

@end
