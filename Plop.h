/*
**  Plop.h
**
**  Copyright (c) 2002
**
**  Author: Ludovic Marcotte <ludovic@Sophos.ca>
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#import <Foundation/Foundation.h>

@class Channel;

@interface Plop : NSObject <NSCoding>
{
  // Owner informations
  NSString *author, *copyright, *email, *web;
  
  // Identity informations
  NSString *title, *plopVersion, *lastModified, *description;
  NSArray *keywords;
  
  // Location informations
  NSURL *defaultLink, *contentSource, *iconSource, *bannerSource, *klipLocation;

  // Setup
  int refresh;

  // Other ivars
  NSString *iconName;
  BOOL active;
  NSRect frame;
  
  Channel *channel;
}


//
// access / mutation methods
//
- (BOOL) isActive;
- (void) setActive: (BOOL) theBOOL;

- (NSURL *) bannerSource;
- (void) setBannerSource: (NSURL *) theBannerSource;

- (NSURL *) contentSource;
- (void) setContentSource: (NSURL *) theContentSource;

- (NSURL *) defaultLink;
- (void) setDefaultLink: (NSURL *) theDefaultLink;

- (NSString *) description;
- (void) setDescription: (NSString *) theDescription;

- (NSRect) frame;
- (void) setFrame: (NSRect) theFrame;

- (NSURL *) iconSource;
- (void) setIconSource: (NSURL *) theIconSource;

- (NSString *) iconName;
- (void) setIconName: (NSString *) theName;

- (int) refresh;
- (void) setRefresh: (int) theRefresh;

- (NSString *) title;
- (void) setTitle: (NSString *) theTitle;

- (Channel *) channel;
- (void) setChannel: (Channel *) theChannel;

- (NSString *) plopVersion;
- (void) setPlopVersion: (NSString *) thePlopVersion;

//
// Other methods
//
- (void) cache;

@end
