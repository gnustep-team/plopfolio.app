/*
**  PlopFolio.h
**
**  Copyright (c) 2002
**
**  Author: Ludovic Marcotte <ludovic@Sophos.ca>
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#import <AppKit/AppKit.h>

@class Plop;

@interface PlopFolio : NSObject
{
  // Outlets
  IBOutlet NSMenu *menu;
  IBOutlet NSMenu *info;
  IBOutlet NSMenu *file;
  IBOutlet NSMenu *edit;
  IBOutlet NSMenu *windows;
  IBOutlet NSMenu *services;

  // ivars
  NSMutableArray *allPlops;
  NSMenu *dockMenu;
}

- (id) init;
- (void) dealloc;


//
// action methods
//
- (IBAction) showPreferencesPanel: (id) sender;

//
// delegate methods
//
- (void) applicationDidFinishLaunching: (NSNotification *) not;
- (void) applicationWillFinishLaunching: (NSNotification *) not;


//
// other methods
//
- (void) showAllPlops;
- (void) showPlop: (Plop *) thePlop;
- (BOOL) synchronize;


//
// access / mutation methods
//
- (NSMutableArray *) allPlops;

//
// static methods
//


@end
