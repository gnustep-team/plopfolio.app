/*
**  PreferencesPanelController.h
**
**  Copyright (c) 2002
**
**  Author: Ludovic Marcotte <ludovic@Sophos.ca>
**
**  This program is free software; you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation; either version 2 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program; if not, write to the Free Software
**  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#import <AppKit/AppKit.h>

@class GSXMLNode;
@class Plop;

@interface PreferencesPanelController : NSWindowController
{
  // Outlets 
  IBOutlet NSTableView *tableView;

  IBOutlet NSPopUpButton *browserPopUpButton;
  IBOutlet NSTextField *browserField;

  // Other ivars
  
}

- (id) initWithWindowNibName: (NSString *) theNibName;
- (void) dealloc;

- (void) initializeFromDefaults;


//
// action methods
//
- (IBAction) configureClicked: (id) sender;
- (IBAction) deleteClicked: (id) sender;
- (IBAction) importClicked: (id) sender;
- (IBAction) selectionInBrowserPopUpButtonHasChanged: (id) sender;

@end


//
// private methods
//
@interface PreferencesPanelController (Private)

- (void) _importKlipUsingPath: (NSString *) thePath;

- (void) _parseKlipInformationsFromNode: (id) theNode
                                   plop: (Plop *) thePlop;

@end


//
// Custom NSButton cell
//
@interface ExtendedButtonCell : NSButtonCell
{
}

@end
